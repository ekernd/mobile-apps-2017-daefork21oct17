package com.wtcweb.mobileapps2017.fredolson;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.wtcweb.mobileapps2017.R;

public class SpinnerEnumLanguages extends AppCompatActivity {

    // Used to easily grab the app context
    // The demomstration online extended the AppCompatActivity class and added
    //  the static context there, then had this class extend that one, but I
    //  "simplified" it to add it to the class of the current screen
    private static android.content.Context context;

    // This will be the enum that populates the spinner
    // Methods are added to turn the resource id into the language-correct string
    enum SelChoices{
        ONE(R.string.ONE),
        TWO(R.string.TWO),
        THREE(R.string.THREE);

        private int resourceId;
        private SelChoices(int id){resourceId = id;}

        @Override
        public String toString(){ // Overriding so that it returns the language-appropriate string
            return SpinnerEnumLanguages.getAppContext().getString(resourceId);
        }
    } //end of enum

    Spinner spnSelChoices;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_enum_languages);

        // enum related - store the current context in a static variable
        SpinnerEnumLanguages.context = getApplicationContext();

        // Grab the spinner and populate it with the enum values
        spnSelChoices = (Spinner)findViewById(R.id.selSpinner);
        spnSelChoices.setAdapter(new ArrayAdapter<SelChoices>(this, R.layout.support_simple_spinner_dropdown_item, SelChoices.values()));
    }

    // for the enum - get the current context
    public static android.content.Context getAppContext(){
        return SpinnerEnumLanguages.context;
    }
}
